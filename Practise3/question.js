/* 

Q.1 Write a function that makes a fetch request to get inventory.json.
Q.2 
    Filter data having main denomination (dollar, pound, rupee) less than 30.

Q3. 
    use Map to get all the denominations from each data.

Q4. 
    Copy data into another object.
    Make sure no references of data is copied.

Q5.
    use Map & also reduce to get just currency and denomination from each data.

*/



const data = [{
    price: {
        currency: "USD",
        denomination: {
            dollar: 28,
            cents: 18
        }
    },
},
{
    price: {
        currency: "AUSD",
        denomination: {
            dollar: 42,
            cents: 12
        }
    }
}, {
    price: {
        currency: "INR",
        denomination: {
            rupee: 35,
            paise: 33
        }
    }
},{
    price: {
        currency: "STERLING POUND",
        denomination: {
            pound: 12,
        }
    }
}];


