// const fetch = require("node-fetch")
// import fetch from 'cross-fetch';
// import fetch from 'node-fetch';

const data = [{
    price: {
        currency: "USD",
        denomination: {
            dollar: 28,
            cents: 18
        }
    },
},
{
    price: {
        currency: "AUSD",
        denomination: {
            dollar: 42,
            cents: 12
        }
    }
}, {
    price: {
        currency: "INR",
        denomination: {
            rupee: 35,
            paise: 33
        }
    }
},{
    price: {
        currency: "STERLING POUND",
        denomination: {
            pound: 12,
        }
    }
}];



const question2 = data.reduce((acc, curr) => {
    let denomination = curr.price.denomination;
    if((denomination['dollar'] < 30) || (denomination['pound'] < 30) || (denomination['rupee'] < 30)){
        acc.push(curr)
    }
    return acc;
}, [])
// console.log(question2);




const question3 = data.map(eachObj => {
    return eachObj.price.denomination;
})
// console.log(question3);



const question4 = data.map(eachObj => ({...eachObj}))
// console.log(question4)
// console.log(data[0]===question4[0]) 


const question5 = data.map(eachObj => eachObj.price).reduce((acc, curr) => {
    acc.push(curr);
    return acc;
}, [])
// console.log(question5);